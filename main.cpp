#include <iostream>
#include <fstream>

using namespace std;

void int_cin_fail_clear();

int main() {
    fstream file;
    int n_files, file_failed = 0;

    cout << "Ile plikow chcesz utworzyc? (1-100) \n";
    cin >> n_files;

    while (n_files < 1 || n_files > 100 || cin.fail()) {
        cout << "Mozna utworzyc min: 1, max: 100 plikow.";
        int_cin_fail_clear();
        cin >> n_files;
    }

    for (int i = 1;file_failed != n_files; i++) {
        file.open(to_string(i) + ".txt", ios::in);
        if (file.fail()) {
            file_failed++;
            file.close();
            file.open(to_string(i) + ".txt", ios::out);
            file.close();
        }
        file.close();
    }

    if (n_files == 1) {
        cout << "Utworzono " << n_files << " plik.";
    } else if (n_files < 5) {
        cout << "Utworzono " << n_files << " pliki.";
    } else {
        cout << "Utworzono " << n_files << " plikow.";
    }
    return 0;
}

void int_cin_fail_clear() {
    cin.clear();
    cin.ignore(numeric_limits<streamsize>::max(), '\n');
}